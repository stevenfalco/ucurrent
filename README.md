µCurrent++ is a modified version of the uCurrent project by David Jones:

https://www.eevblog.com/projects/ucurrent/

Some features from the tinyCurrent project by n-fuse.co are also included:

https://www.n-fuse.co/devices/tinyCurrent-precision-low-Current-Measurement-Shunt-and-Amplifier-Device.html

This is open-source hardware.  This work is licensed under CC BY-SA 4.0:

https://creativecommons.org/licenses/by/4.0/ 

<img src="by-sa.png" >
